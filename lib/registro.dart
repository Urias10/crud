import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() => runApp(registro());

class registro extends StatelessWidget {

  TextEditingController isbn = TextEditingController();
  TextEditingController nombre = TextEditingController();
  TextEditingController autor = TextEditingController();
  TextEditingController editorial = TextEditingController();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text("Libros"),),
        body: Column(
          children: <Widget>[
            TextField(
              controller: isbn,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "ISBN",
              ),
            ),
            TextField(
              controller: nombre,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "Nombre"
              ),
            ),
            TextField(
              controller: autor,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "Autor",
              ),
            ),
            TextField(
              controller: editorial,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                labelText: "Editorial"
              ),
            ),
            RaisedButton(
              onPressed: ()  async {
                Firestore.instance.collection('libros').document()
                    .setData({ 'isbn': "d", 'nombre': "s", 'autor': "df", 'editorial': "scd" });
              },
              padding: EdgeInsets.all(10.0),
              child: Text("Registrar"),
            )
          ],
        ),
      ),
    );
  }
}